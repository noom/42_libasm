#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define BUFFER_LEN 100

size_t  ft_strlen(const char *str);
char    *ft_strcpy(char *dest, const char *src);
int     ft_strcmp(const char *s1, const char *s2);
ssize_t ft_write(int fildes, const void *buf, size_t nbyte);
ssize_t ft_read(int fildes, void *buf, size_t nbyte);
char    *ft_strdup(const char *s1);

int main() {
    printf("================\n");

    printf("ft_strlen\n\
    \"bonjour\" -> %lu, %lu\n\
    \"\" -> %lu, %lu\n\
    \"b\" -> %lu, %lu\n\
    \"bonjour le monde\" -> %lu, %lu\n",
    ft_strlen("bonjour"), strlen("bonjour"),
    ft_strlen(""), strlen(""),
    ft_strlen("b"), strlen("b"),
    ft_strlen("bonjour le monde"), strlen("bonjour le monde"));

    printf("ft_strcpy\n");

    char *src = "bonjour";
    char *dest = (char*)malloc(sizeof(char*) * (ft_strlen(src) + 1));
    ft_strcpy(dest, src);
    printf("dest = \"%s\"\n", dest);

    printf("ft_strcmp\n\
    \"bonjour\", \"bonjour\" -> %d, %d\n\
    \"\", \"bonjour\" -> %d, %d\n\
    \"bonjour\", \"\" -> %d, %d\n\
    \"b\", \"o\" -> %d, %d\n\
    ",
    ft_strcmp("bonjour", "bonjour"), strcmp("bonjour", "bonjour"),
    ft_strcmp("", "bonjour"), strcmp("", "bonjour"),
    ft_strcmp("bonjour", ""), strcmp("bonjour", ""),
    ft_strcmp("b", "o"), strcmp("b", "o"));

    char ft_write_buffer[] = "ft_write hello world\n\0";
    ft_write(1, ft_write_buffer, ft_strlen(ft_write_buffer));

    /* char ft_read_buffer[BUFFER_LEN]; */
    /* ft_read_buffer[BUFFER_LEN - 1] = '\0'; */
    /* ft_write(0, "> ", 2); */
    /* ft_read(1, ft_read_buffer, BUFFER_LEN); */
    /* printf("\rft_read -> %s\r\r", ft_read_buffer); */

    printf("\rft_strdup -> \"%s\"\n", ft_strdup("bonjour"));

    return 0;
}
