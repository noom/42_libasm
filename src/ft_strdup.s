extern _malloc, _ft_strlen, _ft_strcpy

global _ft_strdup

section .text
_ft_strdup:
    push    rdi

    call    _ft_strlen

    mov     rdi, rax

    push    0
    call    _malloc
    add     rsp, 8

    mov     rdi, rax
    pop     rsi
    call    _ft_strcpy
    ret
