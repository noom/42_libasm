global _ft_strcpy

section .text
_ft_strcpy:
    mov     rax, rsi
    mov     r9, -1


cpy:
    inc     r9
    mov     r8b, BYTE [rsi + r9]
    mov     BYTE [rdi + r9], r8b
    cmp     BYTE [rsi + r9], 0
    jne     cpy
    ret
