global _ft_strcmp

section .text
_ft_strcmp:
    mov     r9, -1
    mov     rax, 0

cpy:
    inc     r9
    mov     r8b, BYTE [rsi + r9]

    cmp     BYTE [rdi + r9], r8b
    jl      lesser
    jg      greater

    cmp     BYTE [rdi + r9], 0
    je      exit

    jmp     cpy

lesser:
    dec     rax
    ret

greater:
    inc     rax
    ret

exit:
    ret
