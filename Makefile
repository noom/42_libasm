NAME=asm
OBJS_NAME= \
	ft_strlen.o \
	ft_strcpy.o \
	ft_strcmp.o \
	ft_strdup.o \
	ft_write.o \
	ft_read.o

# SRCS = $(addprefix $(SRCS_DIR)/, $(SRCS_NAME))
OBJS = $(addprefix obj/, $(OBJS_NAME))

$(NAME): $(OBJS)
	ar rcs lib$(NAME).a $(OBJS)

obj/%.o: src/%.s
	mkdir -p obj
	nasm -f macho64 $< -o $@

test: $(NAME)
	gcc test.c -L. -l$(NAME) -o test
	./test
