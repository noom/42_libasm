# Various docs

[pour le c]<https://aaronbloomfield.github.io/pdr/book/x86-64bit-ccc-chapter.pdf>
[instructions cools]<https://home.adelphi.edu/~siegfried/cs174/174l8.pdf>
[base de l'assembleur]<https://aaronbloomfield.github.io/pdr/book/x86-64bit-asm-chapter.pdf>

[liste des appels systeme]<https://opensource.apple.com/source/xnu/xnu-1504.3.12/bsd/kern/syscalls.master>
[super pdf en francais]<http://www.lacl.fr/tan/asm>
[doc officiel apple]<https://developer.apple.com/library/archive/documentation/DeveloperTools/Reference/Assembler/000-Introduction/introduction.html>
