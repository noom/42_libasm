global _ft_strlen

section .text
_ft_strlen:
    ;mov     r8, message
    ;mov     r8, rsi
    mov     rax, 0


counting:
    cmp     BYTE [rdi + rax], 0
    je      end
    inc     rax
    jmp     counting


;write:
;    push    rax
;    push    rdi
;
;    mov     rax, 0x2000004          ; system call for write
;    mov     rdi, 1                  ; file handle 1 is stdout
;    mov     rsi, message            ; address of string to output
;    mov     rdx, 6                  ; number of bytes
;    syscall                         ; invoke operating system to do the write
;
;    pop     rdi
;    pop     rax
;
;    pop     rcx
;    jmp     rcx


end:
;    mov     rax, 0x2000001          ; system call for exit
;    mov     rdi, rbx                ; exit code
;    syscall                         ; invoke operating system to exit


section .data
message:
    db  "debug", 10          ; note the newline at the end
